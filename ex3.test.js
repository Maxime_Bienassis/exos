const max_sum = require('./ex3');

test("test 1", () => {
    expect(max_sum([-5, 3, 7, 15, -17, 8, 12, 1, -6, 8])).toBe(31);
});


test("test 2", () => {
    data = require("./ex3.json");
    data.forEach( (test_case) => {
        expect(
            max_sum(test_case["tab"])
        ).toBe(
            test_case["m"]
        );
    });
});
