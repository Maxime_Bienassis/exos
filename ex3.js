/**
 * max_sum() returns the maximum sum of consecutive
 * elements in *list*
 *
 * example:
 * max_sum([-5, 3, 7, 15, -17, 8, 12, 1, -6, 8]) = 31
 * which is the sum of all but -5
 */

function max_sum(list) {
    // Code here
    return 0;
}

module.exports = max_sum;
