const max_sum = require('./ex1');

test("test 1", () => {
    expect(max_sum([3, 2, 5, 7, 4, 2, 3, 8, 4], 3)).toBe(16);
});

test("test 2", () => {
    expect(max_sum([1, 2, 3], 0)).toBe(0);
});

test("test 3", () => {
    expect(max_sum([], 0)).toBe(0);
});

test("test 4", () => {
    data = require("./ex1.json");
    data.forEach((test_case) => {
        expect(max_sum(
            test_case["tab"], test_case["k"]
        )).toBe(test_case["max_sum"]);
    });
});
