# Javascript exercises

## Ex 1

The objective of the function max_sum is to calculate the maximum
value of the sum of *k* consecutive elements in a *list*.

Example:
```
    max_sum([3, 2, 5, 7, 4, 2, 3, 8, 4], 3) = 16
```
It's the sum of 5,7 and 4

## Ex 2

The objective is to render a tree of comment into html.

Example:
```
[
    {
        "comment": "Career question head. Represent population whatever country down kid operation keep. Run effect cultural.\nDream speak three concern glass. Officer increase form right.",
        "children":
        [
            {
                "comment": "Vote hot natural thought organization positive add. Rise common hit skin. Old sense new expect.",
                "children": null
            },
            {
                "comment": "Gun anything PM specific. Management difference expect travel every guy maintain. Always model yourself born.",
                "children": null
            }
        ]
    }
]
```
Should be rendered as:

```
<ul>
    <li>
        Career question head. Represent population whatever country down kid operation keep. Run effect cultural.\nDream speak three concern glass. Officer increase form right.
        <ul>
            <li>Vote hot natural thought organization positive add. Rise common hit skin. Old sense new expect.</li>
            <li>"Gun anything PM specific. Management difference expect travel every guy maintain. Always model yourself born.</li>
        </ul>
    </li>
</ul>
```

For the sake of simplicity ignore indentation and newlines in the return value.

See `ex2.json` for more examples.

## Ex 3

Exercise 3 is a variant of **Ex 1** where there is no limit on the
number of items *n*, but list elements can be negatives.

Example:
```
    max_sum([-5, 3, 7, 15, -17, 8, 12, 1, -6, 8]) = 31
```

**31** is the sum of all elements but **5**


## Run

To check your results, install jest (with `npm install`) and run
`npm test`

## Notes

- You can use any of the ES6 features, code must run with node v8 and
  npm v5. You must **not** use third party libraries besides of
  **jest**.
- It should not take longer than 2~3 hours to complete all exercises.
- *Ex 1* and *Ex 3* should run in under 1s with arrays of about 100
  000 items.
- For each exercise multiple solutions can be found, try to find the
  most efficient (time wise) then elegant one.
- Have Fun!
