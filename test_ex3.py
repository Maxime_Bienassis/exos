import json

from ex3 import max_sum


def test_case1():
    assert max_sum([-5, 3, 7, 15, -17, 8, 12, 1, -6, 8]) == 31


def test_case2():
    with open("ex3.json") as fo:
        data = json.load(fo)
    for test_case in data:
        assert max_sum(test_case["tab"]) == test_case["m"]
